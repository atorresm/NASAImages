# NASA Images
NASA Images is an android app that randomly selects a pic from NASA Image and Video Library, shows it, provides its description and other related info, and allows to download or share it.

## Download
<a href="https://f-droid.org/packages/atm.nasaimages/">
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
        alt="Get it on F-Droid" height="80">
</a>
<a href="https://gitlab.com/atorresm/NASAImages/-/jobs/artifacts/master/download?job=build">
    <img src="https://hike.in/images/hike5.0/apk.png"
		alt="Download the latest build" height="60">
</a>

## Building from source with Gradle

    ./gradlew android:assembleRelease

## License

>This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See LICENSE file for the full text of the license.

